var cache = []; // cache di elementy, style pattern identity map

function $() { // return oggetto di classe ElementsCollection o Element nel caso di ricerca per id

    var collection = new ElementsCollection();

    for(var i=0; i<arguments.length; i++) {

        var key = arguments[i]; // leggo la stringa di ricerca dell'oggetto
        
        if(cache[key]) { // verifico presenza in cache
            collection.push(cache[key]);
            continue;
        }

        switch(key.charAt(0)) {
            case "#": // ricerca per id
                id = key.split("#").join(""); // str_replace
                return new Element(document.getElementById(id)); // ritorna solo un elemento
                break;
            case ".": // ricerca per classe
                className = key.split(".").join(""); // str_replace
                tags = document.getElementsByTagName("*"); // recupero tutti gli elementi
                for(i=0; i<tags.length; i++) {
                    if(tags[i].className == className) { // se l'elemento corrente ha la classe cercata
                        collection.push(tags[i]);
                    }
                }
                break;
            default: // ricerca per tag
                tags = document.getElementsByTagName(key); // recupero tutti gli elementi
                for(i=0; i<tags.length; i++) {
                    collection.push(tags[i]);
                }
        }
    }

    return collection;
}

function Element(object) {

    this.htmlObject = object;

    this.css = function (property, value) { // se setto value allora ritorno this cosi posso chiamare le funct una dietro l'altra
        
        if(property.constructor == Object) { // se è stato passato un array
            for(key in property) { // foreach key => value
                this.htmlObject.style[key] = property[key];
            }
        } else {
            if(value != undefined) {//gestisco il set
                this.htmlObject.style[property] = value;
                return this;
            }
            //oppure get
            return this.htmlObject.style[property];
        }
        return null;
    }

    //metodo per settare ed ottener attributi di un tag html
    this.attr = function (attribute, value) {
        if(value != undefined) {
            this.htmlObject.setAttribute(attribute, value);
            return this;
        }
        return this.htmlObject.getAttribute(attribute);
    }

    this.val = function () {
        return this.htmlObject.value;
    }

    this.html = function (html) {

        if(html != undefined) {
            this.htmlObject.innerHTML = html;
            return this;
        }
        return this.htmlObject.innerHTML;
    }
}

function ElementsCollection() {

    this.collection = [];
    
    this.push = function (element) {
        this.collection.push(new Element(element));
    }

    this.css = function (property, value) {
        for(i=0; i<this.collection.length; i++) {
            this.collection[i].css(property, value);
        }
    }

    this.attr = function (attribute, value) {
        for(var i=0; i<this.collection.length; i++) {
            this.collection[i].attr(attribute, value);
        }
    }
}

var Window = {

    load: function(lambdaFunction) {
        window.addEventListener('load', lambdaFunction, false);
    }

}

var Ajax = {

    getDataReturnText: function(url, callback) {

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {
            XMLHttpRequestObject = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            XMLHttpRequestObject = new
            ActiveXObject("Microsoft.XMLHTTP");
        }

        if(XMLHttpRequestObject) {
            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function() {
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    callback(XMLHttpRequestObject.responseText);
                    delete XMLHttpRequestObject;
                    XMLHttpRequestObject = null;
                }
            }

            XMLHttpRequestObject.send(null);
        }
    },

    getDataReturnXml: function(url, callback) {

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {
            XMLHttpRequestObject = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            XMLHttpRequestObject = new
            ActiveXObject("Microsoft.XMLHTTP");
        }

        if(XMLHttpRequestObject) {
            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function() {
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    callback(XMLHttpRequestObject.responseXML);
                    delete XMLHttpRequestObject;
                    XMLHttpRequestObject = null;
                }
            }

            XMLHttpRequestObject.send(null);
        }
    },

    postDataReturnText: function(url, data, callback) {

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {
            XMLHttpRequestObject = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            XMLHttpRequestObject = new
            ActiveXObject("Microsoft.XMLHTTP");
        }

        if(XMLHttpRequestObject) {
            XMLHttpRequestObject.open("POST", url);
            XMLHttpRequestObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

            XMLHttpRequestObject.onreadystatechange = function() {
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    callback(XMLHttpRequestObject.responseText);
                    delete XMLHttpRequestObject;
                    XMLHttpRequestObject = null;
                }
            }

            XMLHttpRequestObject.send(data);
        }
    },

    postDataReturnXml: function(url, data, callback) {

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {
            XMLHttpRequestObject = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            XMLHttpRequestObject = new
            ActiveXObject("Microsoft.XMLHTTP");
        }

        if(XMLHttpRequestObject) {
            XMLHttpRequestObject.open("POST", url);
            XMLHttpRequestObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

            XMLHttpRequestObject.onreadystatechange = function() {
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    callback(XMLHttpRequestObject.responseXML);
                    delete XMLHttpRequestObject;
                    XMLHttpRequestObject = null;
                }
            }

            XMLHttpRequestObject.send(data);
        }
    }

}